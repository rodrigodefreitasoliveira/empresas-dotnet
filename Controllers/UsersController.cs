using IoasysWebApi.Domain.Entities;
using IoasysWebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace IoasysWebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ISecurityService _securityService;

        public UsersController(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        [HttpPost("sign_in")]
        public IActionResult Authenticate([FromBody]User user)
        {
            bool result = _securityService.CheckUser(user.Email, user.Password);
            if (result)
            {
                var tokenString = _securityService.GenerateToken();
                return Ok(new { token = tokenString });
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet("teste")]
        public IActionResult Teste()
        {
            return Ok("teste ok");
        }
    }
}