using IoasysWebApi.Domain.Entities;
using IoasysWebApi.Domain.Infrastructure.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysWebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class EnterprisesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public EnterprisesController(ApplicationDbContext context)
        {
            _context = context;
        }

        //Detalhamento
        [HttpGet("{id}")]
        public Enterprise Get(int id)
        {
            return _context.Enterprises.FirstOrDefault(x=>x.Id.Equals(id));
        }

        //Filtro de empresas, caso haja parâmetros.
        //Se não houver, retorna listagem.
        [HttpGet()]
        public IEnumerable<Enterprise> GetAll([FromQuery(Name="enterprise_types")] int? type, [FromQuery(Name="name")] string name)
        {
            var query = _context.Enterprises.AsQueryable();

            if(type != null)
                query = query.Where(x=>x.Type.Equals(type));
            if(!string.IsNullOrEmpty(name))
                query = query.Where(x=>x.Name.Contains(name));

            return query.ToList();
        }
    }
}