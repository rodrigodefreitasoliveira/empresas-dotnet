using System.Net.Mime;
using IoasysWebApi.Domain.Infrastructure.Context;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace IoasysWebApi.Services
{
    public interface ISecurityService
    {
        string GenerateToken();
        bool CheckUser(string email, string password);
    }

    public class SecurityService : ISecurityService
    {
        private readonly ApplicationDbContext _context;
        private IConfiguration _config;

        public SecurityService(ApplicationDbContext context, IConfiguration Configuration)
        {
            _context = context;
            _config = Configuration;
        }

        public bool CheckUser(string email, string password)
        {
            return _context.Users.Any(x=>x.Email.Equals(email) && x.Password.Equals(password));
        }

        public string GenerateToken()
        {
            var issuer = _config["Jwt:Issuer"];
            var audience = _config["Jwt:Audience"];
            var expiry = DateTime.Now.AddMinutes(120);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(issuer: issuer,audience: audience,expires: DateTime.Now.AddMinutes(120),signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);
            return stringToken;
        }
    }
}