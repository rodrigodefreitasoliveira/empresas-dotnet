using IoasysWebApi.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace IoasysWebApi.Domain.Infrastructure.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
                  : base(options)
        { }

        public DbSet<User> Users { get; set; }
         public DbSet<Enterprise> Enterprises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<User>().HasKey(x=>x.Id);
            modelBuilder.Entity<User>().Property(x=>x.Email).IsRequired();
            modelBuilder.Entity<User>().Property(x=>x.Password).IsRequired();

            modelBuilder.Entity<Enterprise>().ToTable("Enterprise");
            modelBuilder.Entity<Enterprise>().HasKey(x=>x.Id);
            modelBuilder.Entity<Enterprise>().Property(x=>x.Name).IsRequired();
            modelBuilder.Entity<Enterprise>().Property(x=>x.Type).IsRequired();
              
        }
    }
}