USE [master]
GO

CREATE DATABASE [AppEmpresa]
GO

USE [AppEmpresa]
GO
CREATE TABLE [User] (
	Id int IDENTITY(1,1) NOT NULL,
	Email varchar(200) COLLATE Latin1_General_CI_AS NOT NULL,
	Password varchar(8) COLLATE Latin1_General_CI_AS NOT NULL,
	PRIMARY KEY (Id)
);
GO

USE [AppEmpresa]
GO

INSERT INTO [User] (Email, Password) VALUES ('testeapple@ioasys.com.br', '12341234');
GO

USE [AppEmpresa]
GO
CREATE TABLE Enterprise (
	Id int IDENTITY(1,1) NOT NULL,
	Name varchar(200) COLLATE Latin1_General_CI_AS NOT NULL,
	Type int NOT NULL,
	PRIMARY KEY (Id)
);

GO

USE [AppEmpresa]
GO

INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 1', 1);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 2', 2);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 3', 3);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 4', 4);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 5', 5);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 6', 6);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 7', 7);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 8', 8);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 9', 9);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa 10', 10);

INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A1', 1);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A2', 1);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A3', 3);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A4', 2);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A5', 5);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A6', 6);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A7', 3);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A8', 3);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A9', 3);
INSERT INTO Enterprise (Name, Type) VALUES ('Empresa A10', 1);
GO