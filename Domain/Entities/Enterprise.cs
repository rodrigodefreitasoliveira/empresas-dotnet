namespace IoasysWebApi.Domain.Entities
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}